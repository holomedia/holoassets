## HoloVerse Assets

This is where we place all our assets
i.e. the stationary material:

- images, logos, icons, video clips

(original files are located into [bublup](https://mystuff.bublup.com/mybublup/#/mystuff/top/folder)

(see also [artifacts](https://sys.artifacts.ai/8jphf/))

Files are organized via subcomponents and extensions

* [krysthal](krysthal)
* [GC-Bank](gc-bank)
* [Consciously concepted](cc)

and

* [icons](icons)
* [svg](svg)
* [webp](webp)
* [mp4](mp4)


Icons and images can be "hotlinked" using [statically.io][st.io] as [CDN]

example :
 
GIThub source:<br>
  ![hotlinked](https://Krysthal-Intelligence-Network.github.io/holoAssets/icons/myccube.png)
  
GHpages CDN:<br>
  ![hotlinked](https://cdn.statically.io/gh/Krysthal-Intelligence-Network/holoAssets/28d654e0/icons/myccube.png)
  
img CDN:<br>
  ![hotlinked](https://cdn.statically.io/img/krysthal-intelligence-network.github.io/holoAssets/icons/myccube.png)


[st.io]: https://cdn.statically.io
[CDN]: https://qwant.com/?q=%26g+content+distribution+network


